//
//  SuperheroFetcher.swift
//  SuperheroesAndLaureates
//
//  Created by Watts,Joseph C on 4/9/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class SuperheroFetcher {
    
    static func fetchSuperheroes() {
        let superheroesUrl = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
        let urlSession = URLSession.shared
        let url = URL(string: superheroesUrl)
        urlSession.dataTask(with: url!, completionHandler: SuperheroFetcher.displaySuperheroes).resume()
    }
    
    static func displaySuperheroes(data: Data?, urlResponse: URLResponse?, error: Error?) -> Void {
        do {
            let decoder = JSONDecoder()
            let squad = try decoder.decode(Squad.self, from: data!)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Updated Heroes"), object: squad.members)
        } catch {
            print(error)
        }
    }
}
