//
//  LaureatesFetcher.swift
//  SuperheroesAndLaureates
//
//  Created by Watts,Joseph C on 4/11/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

class LaureatesFetcher {
    static func fetchLaureates() {
        //let laureatesUrl = "https://api.myjson.com/bins/guc54"
        let laureatesUrl = "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1"
        let urlSession = URLSession.shared
        let url = URL(string: laureatesUrl)
        urlSession.dataTask(with: url!, completionHandler: LaureatesFetcher.displayLaureates).resume()
    }
    
    static func displayLaureates(data: Data?, urlResponse: URLResponse?, error: Error?) -> Void {
        var laureatesMap:[[String:Any]]!
        do {
            var allLaureates:[Laureate] = []
            try laureatesMap = JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [[String: Any]]
            
            if laureatesMap != nil {
                for laureate:[String: Any] in laureatesMap {
                    // line 13,235 doesn't have a surname so we have do guard against it.
                    var surname: String = ""
                    if let parsedSurname = laureate["surname"] {
                        surname = parsedSurname as! String
                    }
                    // but don't worry, there's also ones in there that have literally no first, or last, or anything else associated with them.
                    // ex, line 25931, 25604, 23656
                    
                    var firstName: String = ""
                    if let parsedFirstName = laureate["firstname"] {
                        firstName = parsedFirstName as! String
                    }
                    
                    var born: String = ""
                    if let parsedBorn = laureate["born"] {
                        born = parsedBorn as! String
                    }
                    
                    var died: String = ""
                    if let parsedDied = laureate["died"] {
                        died = parsedDied as! String
                    }
                    
                    let l:Laureate = Laureate(firstName: firstName, surname: surname, birthDate: born, deathDate: died)
                    
                    allLaureates.append(l)
                }
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Updated Laureates"), object: allLaureates)
            }
        } catch {
            print(error)
        }
    }
}
