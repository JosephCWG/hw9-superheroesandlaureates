//
//  SuperHero.swift
//  SuperheroesAndLaureates
//
//  Created by Watts,Joseph C on 4/9/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

struct Hero: Codable {
    var name: String
    var age: Int
    var secretIdentity: String
    var powers: [String]
}
