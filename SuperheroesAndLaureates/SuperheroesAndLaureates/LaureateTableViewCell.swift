//
//  LaureateTableViewCell.swift
//  SuperheroesAndLaureates
//
//  Created by Joseph on 4/11/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import UIKit

class LaureateTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var birthYearLabel: UILabel!
    @IBOutlet weak var deathYearLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
