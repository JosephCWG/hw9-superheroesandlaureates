//
//  Squad.swift
//  SuperheroesAndLaureates
//
//  Created by Watts,Joseph C on 4/11/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation

struct Squad: Codable {
    var squadName: String
    var homeTown: String
    var formed: Int
    var secretBase: String
    var active: Bool
    var members: [Hero] = []
}
