//
//  Laureate.swift
//  SuperheroesAndLaureates
//
//  Created by Watts,Joseph C on 4/11/19.
//  Copyright © 2019 Watts,Joseph C. All rights reserved.
//

import Foundation
struct Laureate {
    var firstName: String
    var surname: String
    var birthDate: String
    var deathDate: String
}
